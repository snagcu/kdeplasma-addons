# Translation for plasma_applet_org.kde.plasma.keyboardindicator.po to Euskara/Basque (eu).
# Copyright (C) 2018-2023 This file is copyright:
# This file is distributed under the same license as the original file.
# KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>.
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2018, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.keyboardindicator\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-22 00:38+0000\n"
"PO-Revision-Date: 2023-07-16 10:08+0200\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.04.0\n"

#: contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Keys"
msgstr "Teklak"

#: contents/ui/configAppearance.qml:37
#, kde-format
msgctxt ""
"@label show keyboard indicator when Caps Lock or Num Lock is activated"
msgid "Show when activated:"
msgstr "Erakutsi aktibatutakoan:"

#: contents/ui/configAppearance.qml:40
#, kde-format
msgctxt "@option:check"
msgid "Caps Lock"
msgstr "Maius giltz"

#: contents/ui/configAppearance.qml:47
#, kde-format
msgctxt "@option:check"
msgid "Num Lock"
msgstr "Zenb giltz"

#: contents/ui/main.qml:87
#, kde-format
msgid "Caps Lock activated"
msgstr "«Maius giltz» aktibatuta"

#: contents/ui/main.qml:90
#, kde-format
msgid "Num Lock activated"
msgstr "«Zk giltz» aktibatuta"

#: contents/ui/main.qml:98
#, kde-format
msgid "No lock keys activated"
msgstr "Giltzatzeko teklak ez daude aktibatuta"

#~ msgid "Num Lock"
#~ msgstr "Zenb giltz"

#~ msgid "%1: Locked\n"
#~ msgstr "%1: Giltzatua\n"

#~ msgid "Unlocked"
#~ msgstr "Giltzatik aske"
